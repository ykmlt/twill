# Twill.io

## Installation
Construction de l'image et lancement du conteneur :
```bash
docker-compose build
docker-compose up
```


```bash
# Installation des bibliothèques PHP
docker-compose exec app composer install

# Lancement des migrations Twill
docker-compose exec app php artisan migrate:fresh

# Création de l'administrateur Twill
docker-compose exec app php artisan twill:superadmin
```

## Accès

### Administration Twill
http://localhost:8080/admin

L'utilisateur et le mot de passe ont été définis par la commande `artisan twill:superadmin`

### Administration base de données
http://localhost:8090

Utilisateur : root <br />
Mot de passe : root <br />
Base de données : twill 
