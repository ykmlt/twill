<?php

namespace App\Twill\Services\MediaLibrary;

use A17\Twill\Services\MediaLibrary\ImageServiceInterface;
use A17\Twill\Services\MediaLibrary\ImageServiceDefaults;

class Glide implements ImageServiceInterface
{
    use ImageServiceDefaults;

    public function getUrl($id, array $params = [])
    {
        $url = $this->getRawUrl($id);
        $query = http_build_query($params);
        if ($query) {
            $url .= '?' . $query;
        }

        return $url;
    }

    public function getUrlWithCrop($id, array $cropParams, array $params = [])
    {
        $params['crop'] = $cropParams['crop_w'] . ',' . $cropParams['crop_h'] . ',' . $cropParams['crop_x'] . ',' . $cropParams['crop_y'];
        return $this->getUrl($id, $params);
    }

    public function getUrlWithFocalCrop($id, array $cropParams, $width, $height, array $params = [])
    {
        $params['crop'] = $cropParams['crop_w'] . ',' . $cropParams['crop_h'] . ',' . $cropParams['crop_x'] . ',' . $cropParams['crop_y'];
        return $this->getUrl($id, $params);
    }

    public function getLQIPUrl($id, array $params = [])
    {
        return $this->getUrl($id, $params);
    }

    public function getSocialUrl($id, array $params = [])
    {
        return $this->getUrl($id, $params);
    }

    public function getCmsUrl($id, array $params = [])
    {
        return $this->getUrl($id, $params);
    }

    public function getRawUrl($id)
    {
        $url = str_replace('uploads/', 'img/', $id);
        return '/' . $url;
    }

    public function getDimensions($id)
    {
        return null;
    }
}
